# README #
This is written at 2020/11.  

### The *.dsc source code can be downloaded from here ###
https://ftp.yz.yamagata-u.ac.jp/pub/linux/ubuntu/archives/pool/universe/n/nim/

### Building the *.deb package of nim compiler for i386 archtecture ###
-  Debian10 (buster) (or Ubunutu 18.04 or later) for i386
    1. $ pwd /home/username
    1. $ mkdir work
    1. $ cd work
    1. $ sudo apt-get install build-essential devscripts
    1. $ dget -u https://ftp.yz.yamagata-u.ac.jp/pub/linux/ubuntu/archives/pool/universe/n/nim/nim_1.4.0-1.dsc
    1. $ sudo apt-get build-dep nim
    1. $ sudo apt install help2man
    1. $ cd ___nim-1.4.0___
    1. $ time dpkg-buildpackage -r -uc -b -d
        - Compilation time,
            - real    43m8.583s
            - user    34m46.442s
            - sys     4m39.807s

### Download *.deb packages for nim compiler for i386 archtecture ###
* [nim_1.4.0-1_i386_debian10_buster.deb](https://bitbucket.org/dinau/nim-data/downloads/nim_1.4.0-1_i386_debian10_buster.deb)
* [nim_1.2.0-1_i386_bionic_18.04.deb](https://bitbucket.org/dinau/nim-data/downloads/nim_1.2.0-1_i386_bionic_18.04.deb)

